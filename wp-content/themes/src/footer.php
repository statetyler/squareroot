		</div> <!-- END #container -->

		<footer id="site-footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

			<div id="inner-footer" class="wrap">

				<div id="footer_logo" data-appear="fade-right" itemscope itemtype="http://schema.org/Organization">
					<a href="<?php echo home_url(); ?>" rel="nofollow" title="<?php bloginfo('name'); ?> | Home">
						<?php include_svg('src-logo'); ?>
					</a>
				</div>

				<nav class="footer-nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<?php wp_nav_menu(array(
						'container' => '',                              // enter '' to remove nav container (just make sure .footer-nav in _base.scss isn't wrapping)
						'container_class' => '',                        // class of container (should you choose to use it)
						'menu' => __( 'Footer nav', 'bonestheme' ),   	// nav name
						'menu_class' => 'footer-menu',                  // adding custom nav class
						'theme_location' => 'footer-nav',             	// where it's located in the theme
						'before' => '',                                 // before the menu
						'after' => '',                                  // after the menu
						'link_before' => '',                            // before each link
						'link_after' => '',                             // after each link
						'depth' => 0,                                   // limit the depth of the nav
						'fallback_cb' => 'bones_footer_nav_fallback'  	// fallback function
					)); ?>

					<?php wp_nav_menu(array(
						'container' => '',                              	// enter '' to remove nav container (just make sure .footer-nav in _base.scss isn't wrapping)
						'container_class' => '',                        	// class of container (should you choose to use it)
						'menu' => __( 'Footer Sub Nav', 'bonestheme' ),   	// nav name
						'menu_class' => 'footer-menu footer-sub-menu',      // adding custom nav class
						'theme_location' => 'footer-sub-nav',             	// where it's located in the theme
						'before' => '',                                 	// before the menu
						'after' => '',                                  	// after the menu
						'link_before' => '',                            	// before each link
						'link_after' => '',                             	// after each link
						'depth' => 0,                                   	// limit the depth of the nav
						'fallback_cb' => 'bones_footer_sub_nav_fallback'  	// fallback function
					)); ?>

					<?php // display_social_media_links(); ?>
				</nav> <!-- END footer-nav -->

				<p class="source-org copyright">
					<span>Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.&nbsp;</span>
					<span>All Rights Reserved.&nbsp;</span>
					<span><a href="http://statebuilt.com" target="_blank" title="Made with Love in Austin, TX. Website Built by State.">Site by State</a>.</span>
				</p> <!-- END copyright -->

			</div> <!-- END inner-footer -->

		</footer> <!-- END footer -->

		<?php popup(); ?>

		<?php // all js scripts are loaded in library/bones.php ?>
		
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->