<?php get_header(); ?>

	<section class="hero-default <?php hero_class(); ?>" <?php hero_image(); ?>>
		<?php hero_overlay(); ?>
		<div class="hero-custom-title wrap block">
			<?php hero_title(); ?>
			<?php hero_subtitle(); ?>
		</div>
	</section> <!-- END hero -->

	<div id="content">
		<div id="inner-content">

			<section class="wrap block">

				<img src="<?php echo get_stylesheet_directory_uri(); ?>/library/images/404.png" alt="Thankfuly your data is 'safe' on the cloud.">

				<div>
					<a class="btn btn-border mar-top mar-right" href="/home/" title="Back to the Homepage">Back to Homepage</a>
					<a class="btn mar-top" href="/contact/" title="Go to the contact page">Contact Us</a>
				</div>
				
			</section> <!-- END new-section -->

		</div> <!-- END inner-content -->
	</div> <!-- END content -->

<?php get_footer(); ?>
