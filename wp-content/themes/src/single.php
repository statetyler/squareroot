<?php get_header(); ?>

	<section class="hero-default <?php hero_class(); ?>" <?php hero_image(); ?>>
		<?php hero_overlay(); ?>
		<div class="hero-custom-title wrap block">
			<h1 data-appear="fade-right" data-appear-delay="100">Blog Post</h1>
		</div>
	</section> <!-- END hero -->

	<div id="content">
		<div id="inner-content">

			<main id="main" class="cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'post-formats/format', get_post_format() ); ?>

				<?php endwhile; ?>

				<?php else : ?>

				<article id="post-not-found" class="hentry cf">
					<header class="article-header">
						<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
					</header>
					<section class="entry-content">
						<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
					</section>
					<footer class="article-footer">
						<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
					</footer>
				</article>

				<?php endif; ?>

			</main>

		</div> <!-- END inner-content -->
	</div> <!-- END content -->

<?php get_footer(); ?>