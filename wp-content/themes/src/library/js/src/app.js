import './bones-config';

import '../../scss/style.scss';

import './jquery-appear';

import './mobile-nav';

import './gsap';

import {genericSocialShare} from './social-sharing';
window.genericSocialShare = genericSocialShare;



import Vue from 'vue';

const app = new Vue({
    el        : '#container',
    components: {
        Hello: () => import('./VueComponents/Hello.vue')
    }
});



jQuery(document).ready(function($) {	
    function scrollToSection(event) {
        event.preventDefault();

        var $section = $($(this).attr('href'));
        
        $('html, body').animate({
            scrollTop: $section.offset().top
        }, 500);
    }
    $('[data-scroll]').on('click', scrollToSection);
}(jQuery));