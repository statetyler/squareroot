/////////////////////////////////////////////////////
// Scroll to the desired section on click
/////////////////////////////////////////////////////
/*
  	USAGE:
	Add the `data-scroll` attribute to <a>
	tags that you would like to cause the scroll.
	The section being scrolled to must have an `id`
	equal to the `href` of the <a>.


   	EXAMPLE:
    <a data-scroll href="#section-id">Some Text</a>

    will scroll to:

    <div id="section-id">Section Content</div>
*/
/////////////////////////////////////////////////////