import {Tweenmax} from 'gsap';



TweenMax.staggerFrom(".link-card", 1, {
    scale: 1.1,
    delay: 0.2,
    ease:Bounce.easeOut,
}, 0.5);