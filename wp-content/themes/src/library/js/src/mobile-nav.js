/////////////////////////////////////////////////////
// Mobile Navigation
/////////////////////////////////////////////////////

jQuery(document).ready(function($) {
	var $navBtn = $('.nav-button');
	var activeClass = 'nav-active';

	$navBtn.on('click', function() {
		$('body').toggleClass(activeClass);
	});

	// temporarily disable the transitions on menu when resizing
	$(document).ready(function() {
		window.onresize = resizePage; 
		resizePage();
	});

	function restoreTransition() {
		$("nav.header-nav, nav.header-nav *").css( "transition-property", "" );
	};
	
	function resizePage(){
		var width = (window.innerWidth > 0) ? window.innerWidth : document.documentElement.clientWidth;
	
		if(width <= 768) {
			$("nav.header-nav, nav.header-nav *").css( "transition-property", "none" ); // temporarily disable the transition
			setTimeout(restoreTransition, 100); // restore the transition after timeout
		};
	}

}(jQuery));