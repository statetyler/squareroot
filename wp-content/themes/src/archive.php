<?php get_header(); ?>

	<section class="hero-default <?php hero_class(); ?>" <?php hero_image(); ?>>
		<?php hero_overlay(); ?>
		<div class="hero-custom-title wrap block">
			<h1 data-appear="fade-right" data-appear-delay="100">
			<?php
				the_archive_title( '<span class="page-title">', '</span>' );
				the_archive_description( '<span class="taxonomy-description">', '</span>' );
			?>
			</h1>
			<?php hero_subtitle(); ?>
		</div>
	</section> <!-- END hero -->

	<div id="content">
		<div id="inner-content" class="wrap row cf">

			<main id="main" class="has-sidebar cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">							
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" class="wrap-no-gutter row-no-top" role="article">

					<header class="article-header">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h3 class="entry-title"><?php the_title(); ?></h3></a>

						<p class="byline vcard">
							<?php printf( __( 'Posted', 'bonestheme' ).' %1$s %2$s',
								/* the time the post was published */
								'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>',
								/* the author of the post */
								'<span class="by">'.__('by', 'bonestheme').'</span> <span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
							); ?>
						</p>
					</header>

					<section class="article-content">

						<?php the_post_thumbnail('full'); ?>

						<section class="entry-excerpt">
							<?php the_excerpt(); ?>
						</section>

					</section>

					<footer class="article-footer">
						<div class="footer-meta">
							<?php the_tags( '<p class="footer-tags"> ', '', '</p>' ); ?>

							<?php printf( '<p class="footer-category">' . __('Category', 'bonestheme' ) . ' | %1$s</p>' , get_the_category_list(', ') ); ?>
						</div>
					</footer>

				</article>

			<?php endwhile; ?>
			
				<?php bones_page_navi(); ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry cf">
					<header class="article-header">
						<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
					</header>
					<section class="entry-content">
						<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
					</section>
					<footer class="article-footer">
						<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
					</footer>
				</article>

			<?php endif; ?>
			</main> <!-- END main-content -->

			<aside class="sidebar cf bg-off-white text-white">
				<h5>Sidebar Here</h5>
				<?php // get_sidebar(); ?>
			</aside>

		</div> <!-- END inner-content -->
	</div> <!-- END content -->

<?php get_footer(); ?>
