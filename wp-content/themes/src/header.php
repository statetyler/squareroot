<!doctype html>

<!--[if lt IE 7]><html <?php // language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php // language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php // language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>

		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php // echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
    	<meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<header id="site-header" class="overlay-header" role="banner" itemscope itemtype="http://schema.org/WPHeader">
			<div id="inner-header" class="wrap">

				<div id="logo" data-appear="fade-right" itemscope itemtype="http://schema.org/Organization">
					<a href="<?php echo home_url(); ?>" rel="nofollow" title="<?php bloginfo('name'); ?> | Home">
						<?php include_svg('src-logo'); ?>
					</a>
				</div>

				<nav class="header-nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
					<div class="inner-header-nav">
						<!-- <div data-appear="fade-left">
							<?php // display_social_media_links(); ?>
						</div> -->
						
						<div data-appear="fade-left">
							<?php wp_nav_menu( [
								'container'       => false, 								// remove nav container
								'container_class' => '', 									// class of container (should you choose to use it)
								'menu'            => __('The Main Menu', 'bonestheme'), 	// nav name
								'menu_class'      => 'header-menu', 						// adding custom nav class
								'theme_location'  => 'main-nav',  							// where it's located in the theme
								'before'          => '', 				  					// before the menu
								'after'           => '', 				  					// after the menu
								'link_before'     => '', 			 	  					// before each link
								'link_after'      => '', 				  					// after each link
								'depth'           => 0,  				  					// limit the depth of the nav
								'fallback_cb'     => ''  				  					// fallback function (if there is one)
							] ); ?>
						</div>
					</div>
				</nav> <!-- END header-bar -->

				<div class="nav-button" data-appear="fade-left">
					<label for="Menu">Menu</label>
					<span class="i1"></span>
					<span class="i2"></span>
					<span class="i3"></span>
				</div>

			</div> <!-- END inner-header -->
		</header> <!-- END site-header -->

    	<div id="container">