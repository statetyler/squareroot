<?php get_header(); ?>

	<section class="hero-default <?php hero_class(); ?>" <?php hero_image(); ?>>
		<?php hero_overlay(); ?>
		<div class="hero-custom-title wrap block">
			<?php hero_title(); ?>
			<?php hero_subtitle(); ?>
			<?php hero_intro(); ?>
		</div>
	</section> <!-- END hero -->

	<div id="content">
		<div id="inner-content">

			<main id="main" class="wrap row" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
			<?php if (get_field('page_title')): ?>
				<h2><?php the_field('page_title'); ?></h2>
			<?php endif; ?>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
			</main> <!-- END main-content -->

			<section class="wrap block">
			
			</section> <!-- END new-section -->

		</div> <!-- END inner-content -->
	</div> <!-- END content -->

<?php get_footer(); ?>