
			<?php
			/*
			 * Post Template: Standard Format
			 */
			?>

			<article id="post-<?php the_ID(); ?>" class="wrap row" role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

				<header class="article-header align-center mw-narrow">

					<div class="post-topbar">
						<div class="profile-pic">
							<div class="background-img" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/library/images/blank-profile-picture-640.png'); ">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 64 ); ?>
							</div>
						</div>

						<p class="entry-meta vcard post-meta">
							<?php printf( __( '', 'bonestheme' ).' %1$s %2$s',
								/* the time the post was published */
								'<time class="updated entry-time" datetime="' . get_the_time('Y-m-d') . '" itemprop="datePublished">' . get_the_time(get_option('date_format')) . '</time>',
								/* the author of the post */
								'<span class="entry-author author" itemprop="author" itemscope itemptype="http://schema.org/Person">' . get_the_author_link( get_the_author_meta( 'ID' ) ) . '</span>'
							); ?>
						</p>

						<?php share_icons(); ?>	
					</div>

					<h1 class="entry-title" itemprop="headline" rel="bookmark"><?php the_title(); ?></h1>

				</header> <?php // end article header ?>

				<section class="article-content margin-bottom" itemprop="articleBody">
					<div class="entry-content">
						<?php the_content(); ?>
					</div>

					<?php
						wp_link_pages( array(
							'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
							'after'       => '</div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
						) );
					?>
				</section> <?php // end article section ?>

				<footer class="article-footer align-center mw-narrow">
					<div class="footer-meta">
						<?php the_tags( '<p class="footer-tags"> ', '', '</p>' ); ?>

						<?php printf( '<p class="footer-category">' . __('Category', 'bonestheme' ) . ' | %1$s</p>' , get_the_category_list(', ') ); ?>
					</div>

					<div class="xtr-margin-top">
						<?php comments_template(); ?>
					</div>
				</footer> <?php // end article footer ?>

			</article> <?php // end article ?>
