# STATE Boilerplate
A lightweight theme for WordPress Development, built by STATE.
Based on Bones by Eddie Machado.

### Installation
1. Copy entire theme to `your-site > wp-content > themes`.
2. In terminal, cd to `your-site > wp-content > themes > state-boilerplate`.
3. Run `npm install`
4. You can rename the `state-boilerplate` directory to whatever you'd like. Then, open `style.css` in the root of the theme and change the meta data at the top of the file ie. Theme Name, Author, etc.
5. Run `gulp` in the terminal. This will watch your .scss and .js files and compile them for you.

### Build Scripts
STATE Boilerplate uses Gulp, SASS (SCSS), and Autoprefixer to compile your styles and javascript into single files, with all necessary vendor-prefixes automatically created. You don't need to worry about prefixing your CSS rules.

---

#### Bones
A Lightweight Wordpress Development Theme

Bones is designed to make the life of developers easier. It's built
using HTML5 & has a strong semantic foundation.
It's constantly growing so be sure to check back often if you are a
frequent user. I'm always open to contribution. :)

Designed by Eddie Machado
http://themble.com/bones

License: WTFPL
License URI: http://sam.zoy.org/wtfpl/
Are You Serious? Yes.

##### Special Thanks to:
Paul Irish & the HTML5 Boilerplate
Yoast for some WP functions & optimization ideas
Andrew Rogers for code optimization
David Dellanave for speed & code optimization
and several other developers. :)

##### Submit Bugs & or Fixes:
https://github.com/eddiemachado/bones/issues

To view Release & Update Notes, read the CHANGELOG.md file in the main folder.

For more news and to see why my parents constantly ask me what I'm
doing with my life, follow me on twitter: @eddiemachado

##### Helpful Tools & Links

Yeoman generator to quickly install Bones Wordpress starter theme into your Wordpress theme folder
by 0dp
https://github.com/0dp/generator-wp-bones


