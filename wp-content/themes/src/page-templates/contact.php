<?php
/*
 * Template Name: Contact
 */
  get_header();
?>

	<section class="hero-default <?php hero_class(); ?>" <?php hero_image(); ?>>
		<?php hero_overlay(); ?>
		<div class="hero-custom-title wrap block">
			<?php hero_title(); ?>
			<?php hero_subtitle(); ?>
            <?php hero_intro(); ?>
		</div>
	</section> <!-- END hero -->

	<div id="content">
		<div id="inner-content">

            <section class="wrap block flex-row">

                <main id="main" class="flex-half-column" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <?php the_content(); ?>

                <?php endwhile; endif; ?>
                </main> <!-- END main-content -->

                <div class="flex-half-column">
                    <div class="info-stack">
                        <div data-appear="fade-left" data-appear-delay="200">
                            <h5><?php bloginfo( 'name' ); ?></h5>
                            <a href="<?php the_field('address_link', 'option'); ?>">
                                <?php the_field('address', 'option'); ?>
                            </a>
                        </div>

                        <span class="dividing-line" data-appear="fade-in"></span>
                        
                        <div data-appear="fade-right" data-appear-delay="200">
                            <a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
                            <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                        </div>
                    </div>

                    <div class="margin-top at-480-center-content" data-appear="fade-in" data-appear-delay="300">
                        <?php display_social_media_links(); ?>
                    </div>
                </div> <!-- END info-stack -->
                    
			</section> <!-- END contact info / forms -->

		</div> <!-- END inner-content -->
    </div> <!-- END content -->

<?php get_footer(); ?>