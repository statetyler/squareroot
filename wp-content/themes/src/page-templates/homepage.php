<?php
/*
* Template Name: Homepage
*/
	get_header();
?>

	<section class="hero hero-home <?php hero_class(); ?>">
		<div class="hero-left-sidebar">
			<div class="hero-inner">
				<div class="hero-content">
					<?php hero_intro(); ?>
				</div>
				<div class="hero-image" <?php hero_image(); ?>></div>
			</div>
		</div>
	</section> <!-- END hero -->

	<?php if (have_posts()): ?>
	<main class="wrap block flex-row page-intro">
	
		<div class="flex-half-col content-wrap">
			<div class="wysiwyg">
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
			</div>

			<?php if( have_rows('small_title_blurbs') ): $i = 1; ?>
			<div class="small-title-blurbs margin-top">
			<?php while ( have_rows('small_title_blurbs') ) : the_row(); ?>

				<div class="blurb">
					<div class="blurb-icon">
						<img src="<?php the_sub_field('icon'); ?>" alt="Icon">
					</div>
					<div class="blurb-content">
						<h4><?php the_sub_field('title'); ?></h5>
						<p><?php the_sub_field('copy'); ?></p>
					</div>
				</div>

			<?php $i+=1; endwhile; ?>
			</div>
			<?php endif; ?>
		</div>

		<div class="flex-half-col image-wrap">
			<div class="fig-wrap">
				<figure>
					<?php the_post_thumbnail( 'full' ); ?>
				</figure>
				<div class="accent-bars">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>

	</main> <!-- END main-content -->
	<?php endif; ?>
    
	
	<?php if( have_rows('split_content') ): $i = 1; ?>
	<section class="block">
	<?php while ( have_rows('split_content') ) : the_row(); ?>

		<div class="flex-row split-content">

			<div class="flex-half-col image-wrap">
				<div class="bg-img" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/library/images/mountains-norway.jpg')"></div>
			</div>

			<div class="flex-half-col content-wrap">
				<div class="read-more-block">
					<h4><?php the_sub_field('title'); ?></h4>
					<p><?php the_sub_field('copy'); ?></p>
					<a href="<?php the_sub_field('link'); ?>">read more</a>
				</div>
			</div>
			
		</div>

	<?php $i+=1; endwhile; ?>
	</section> <!-- END split_content -->
	<?php endif; ?>

<?php get_footer(); ?>