<?php
/*
* Template Name: Values
*/
	get_header();
?>

	<section class="hero <?php hero_class(); ?>">
		<div class="hero-left-sidebar">
			<div class="hero-inner">
				<div class="hero-content">
					<?php hero_title(); ?>
					<?php hero_intro(); ?>
				</div>
				<div class="hero-image" <?php hero_image(); ?>></div>
			</div>
		</div>
	</section> <!-- END hero -->

	<?php if (have_posts()): ?>
	<main class="wrap block flex-row page-intro">
	
		<div class="flex-half-col content-wrap">
			<!-- <div class="wysiwyg">
			<?php while (have_posts()) : the_post(); ?>
				<?php // the_content(); ?>
			<?php endwhile; ?>
			</div> -->

			<?php if( have_rows('title_blurbs') ): $i = 1; ?>
			<div class="title-blurbs">
			<?php while ( have_rows('title_blurbs') ) : the_row(); ?>

				<div class="blurb">
					<div class="blurb-content">
						<h4><?php the_sub_field('title'); ?></h5>
						<p><?php the_sub_field('copy'); ?></p>
					</div>
				</div>

			<?php $i+=1; endwhile; ?>
			</div>
			<?php endif; ?>
		</div>

		<div class="flex-half-col image-wrap">
			<div class="fig-wrap">
				<figure>
					<?php the_post_thumbnail( 'full' ); ?>
				</figure>
				<div class="accent-bars">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
		</div>

	</main> <!-- END main-content -->
	<?php endif; ?>

	<?php $statement = get_field('statement_row'); if( $statement ): ?>
	<section class="block row statement-row">

		<div class="wrap">
			<h4><?php echo $statement['title']; ?></h4>
			<?php echo $statement['copy']; ?>
		</div>

	</section> <!-- END statement-row -->
	<?php endif; ?>

<?php get_footer(); ?>