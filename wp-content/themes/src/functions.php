<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once 'library/bones/bones.php';

// Other bones functions for getting it started
require_once 'library/bones/launch-bones.php';

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );



/************************
ADDITIONAL THEME ADD-ONS
************************/

/* Custom Fields */
require_once 'includes/acf-fields.php';

/* Theme Options Page */
require_once 'includes/theme-options.php';

/* Popup */
require_once 'includes/popup.php';

/* Hero Functions */
require_once 'includes/hero-functions.php';

/* Include SVG */
require_once 'includes/include-svg.php';

/* Parse Video */
require_once 'includes/parse-video.php';

/* Social Media Links */
require_once 'includes/social-media-links.php';

/* Social Sharing Buttons */
require_once 'includes/social-sharing.php';

/* Asset Manifest */
require_once 'includes/get-asset-manifest.php';