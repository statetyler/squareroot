<?php
/*
 Template Name: Custom Page
*/
get_header(); ?>

<!-- I've been using this template to build the page in the wysiwyg -->

<section class="hero-default <?php hero_class(); ?>" <?php hero_image(); ?>>
	<?php hero_overlay(); ?>
	<div class="hero-custom-title wrap block">
		<?php hero_title(); ?>
		<?php hero_subtitle(); ?>
		<?php hero_intro(); ?>
	</div>
</section> <!-- END hero -->

<div id="content">
	<div id="inner-content">

		<main id="main" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
		</main> <!-- END main-content -->

	</div> <!-- END inner-content -->
</div> <!-- END content -->

<?php get_footer(); ?>