const path               = require("path"),
      webpack            = require('webpack'),
      autoprefixer       = require('autoprefixer'),
      ExtractTextPlugin  = require("extract-text-webpack-plugin"),
      CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry  : "./library/js/src/app.js",
    output : {
        filename: "scripts.[chunkhash].js",
        path    : path.join(__dirname, "/build/"),
        publicPath: getPublicDir(path),
        chunkFilename: '[name].[chunkhash].js'
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    module : {
        rules: [
            {
                test   : /\.vue$/,
                loader : 'vue-loader',
                options: {
                    loaders: {
                        js     : 'babel-loader',
                        options: {
                            presets: ["env"],
                            plugins:['babel-plugin-dynamic-import-webpack']
                        }
                    }
                }
            },
            {
                test   : /\.js$/,
                exclude: /(node_modules)/,
                use    : {
                    loader : "babel-loader",
                    options: {
                        presets: ["env"],
                        plugins:['babel-plugin-dynamic-import-webpack']
                    }
                }
            },
            {
                test: /\.scss$/,
                use : ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use     : [
                        {
                            loader : 'css-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                        {
                            loader : 'postcss-loader',
                            options: {
                                sourceMap: true,
                                plugins  : function()
                                {
                                    return [autoprefixer]
                                }
                            }
                        },
                        {
                            loader : 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        },
                    ],
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '../build/style.[chunkhash].css'
        }),

        new CleanWebpackPlugin(['build'], { root: __dirname, }),

        //Write Manifest
        function()
        {
            this.plugin('done', stats =>
            {
                require('fs').writeFileSync(
                    path.join(__dirname, '/build/manifest.json'),
                    JSON.stringify(makeManifest(stats))
                );
            });
        }
    ],
    devtool: "source-map",
};

function makeManifest(stats) {
    let chunk = stats.toJson().assetsByChunkName;
    let javaScriptFile = chunk.main[0];
    let cssFile = chunk.main[1].replace('../build/', '');
    return {
        scripts: javaScriptFile,
        styles : cssFile
    };
}

function getPublicDir(path) {
    let absolutePath = path.join(__dirname, "/build/");
    let index = absolutePath.indexOf('wp-content');

    return absolutePath.substr(index);
}
