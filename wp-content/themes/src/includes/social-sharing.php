<?php // Custom Social Share Buttons

function get_share_url_enc()
{
    $share_url       = get_permalink();
    $share_chars     = [':', '/'];
    $share_chars_enc = ['%3A', '%2F'];

    return str_replace($share_chars, $share_chars_enc, $share_url);
}

function facebook_share()
{
    $share_url_enc = get_share_url_enc();
    ?>
    <a class="icon-fb" 
       href="javascript:void(0)" 
       onclick="javascript:window.genericSocialShare('http://www.facebook.com/sharer.php?u=<?php echo $state_share_url_enc; ?>')" 
       title="Share on Facebook">
        <i class="fa fa-facebook" aria-hidden="true"></i>
    </a>
    <?php
}

function twitter_share()
{
    $share_url_enc = get_share_url_enc();
    ?>
    <a class="icon-tw" 
       href="javascript:void(0)" 
       onclick="javascript:window.genericSocialShare('http://twitter.com/share?text=<?php echo get_the_title(); ?>&amp;url=<?php echo $state_share_url_enc; ?>')" 
       title="Tweet">
        <i class="fa fa-twitter" aria-hidden="true"></i>
    </a>
    <?php
}

function email_share()
{
    ?>
    <a class="icon-mail"
       href="mailto:person@mail.com? &amp;subject=Check out this article! &amp;body=<?php echo get_permalink() ?>"
       title="Share with an email">
        <i class="fa fa-envelope"></i>
    </a>
    <?php
}

function go_to_comments()
{
    ?>
    <a class="icon-comment"
       data-scroll
       href="#commentform"
       title="Go to comment section">
        <i class="fa fa-commenting"></i>
    </a>
    <?php
}

function share_icons()
{
    ?>
    <ul class="social-sharing">
        <label for="Social Sharing">Share</label>
        <li class="fb-share"><?php facebook_share(); ?></li>
        <li class="tw-share"><?php twitter_share(); ?></li>
        <li class="email-share"><?php email_share(); ?></li>
        <li class="go-to-comment"><?php go_to_comments(); ?></li>              
    </ul>
    <?php
}