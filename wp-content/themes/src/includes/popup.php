<?php // POPUP
function popup() {
	$popupActive = get_field('popup_active', 'option');
    $pagesHidden = get_field('pages_hidden', 'option');
	$hidePopup = false;

    if ($pagesHidden) {
        foreach($pagesHidden as $page) {
            if(is_page($page) || is_single($page)) {
                $hidePopup = true;
            break;
            }
        }
    }

    if(!$hidePopup && $popupActive):

    // Vars for cleanliness
    $popupImage = get_field('popup_image', 'option');
    $popupTitle = get_field('popup_title', 'option');
    $popupText = get_field('popup_text', 'option');
    $downloadActive = get_field('download_active', 'option');
    $downloadText = get_field('download_text', 'option');
?>
    
	<div class="popup-overlay"></div>
	<div class="popup widget_mc4wp_widget">
        
        <?php if($popupImage): ?>
		<div class="popup-image" style="background-image: url('<?php echo $popupImage; ?>');"></div>
        <?php endif; ?>
        
        <a class="popup-close">&times;</a>

		<div class="popup-content">
			<?php if($popupTitle): ?>
			<h3><?php echo $popupTitle; ?></h3>
			<?php endif; ?>

			<?php if($popupText): ?>
			<p><?php echo $popupText; ?><p>
			<?php endif; ?>

			<div class="popup-form">
			<?php echo do_shortcode(get_field('mc_shortcode', 'option')); ?>
			</div>
		</div> <!-- END popup-content -->

        <?php if($downloadActive): ?>
		<div class="popup-download" style="display: none;">
			<?php if($downloadText): ?>
			<h4><?php echo $downloadText; ?></h4>
			<?php endif; ?>

			<?php
				if(get_field('the_download', 'option')) {
					$theDownload = get_field('the_download', 'option');
				} else {
					$theDownload = '#0';
				}
			?>

			<a href="<?php echo $theDownload; ?>" class="btn" download>Download</a>
        </div> <!-- END popup-download -->
        <?php endif; ?>

  	</div> <!-- END popup -->

	<script>
		jQuery(document).ready(function() {

            function emailPopup() {

                var seconds = <?php the_field('popup_delay', 'option'); ?>; // seconds before firing
                var minutes = <?php the_field('popup_timeout', 'option'); ?>; // minutes between loading

                var popup = jQuery('.popup');
                var overlay = jQuery('.popup-overlay');
                var close = jQuery('.popup-close');
                var now = (new Date()).getTime();
                var lastTime = 0;
                var lastTimeStr = localStorage['lastTime'];

                if (lastTimeStr) lastTime = parseInt(lastTimeStr, 10);
                if (now - lastTime >  minutes * 60000) {

                    setTimeout(function() {
                        popup.addClass('js-popup-active');
                        overlay.addClass('js-popup-active');
                    }, seconds * 1000);

                    close.click(function() {
                        popup.removeClass('js-popup-active');
                        overlay.removeClass('js-popup-active');
                    });

                    overlay.click(function() {
                        popup.removeClass('js-popup-active');
                        overlay.removeClass('js-popup-active');
                    });

                    localStorage['lastTime'] = '' + now;
                }
            }

            emailPopup();

            <?php if($downloadActive): ?>
		    // Show the download button after the form is submitted
            mc4wp.forms.on('success', function(form) {
                console.log('SUCCESS');
                $('.popup-form-fields').fadeOut('500');
                $('.popup-top').fadeOut('500');
                $('.popup-download').fadeIn('500');
            });
            <?php endif; ?>
		});
	</script>

<?php endif; } // END popup ?>