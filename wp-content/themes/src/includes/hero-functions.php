<?php

function get_hero_post_id()
{
    global $wp_query;

    return $wp_query->post->ID;
}

function get_hero_image()
{
    $postID = get_hero_post_id();

    return get_field('hero_image', $postID);
}

function get_hero_overlay()
{
    $postID = get_hero_post_id();

    return get_field('show_overlay', $postID);
}

// HERO IMAGE
function hero_image()
{
    $hero_img = get_hero_image();

    if($hero_img) {
        echo 'style="background-image: url(\'' . $hero_img['url'] . '\');"';
    }
}

// HERO CLASS - used to add a class to hero if image is present
function hero_class()
{
    $hero_img = get_hero_image();

    if($hero_img) {
        echo 'has-image';
    }
    else {
        echo 'no-image';
    }
}

// HERO OVERLAY
function hero_overlay()
{
    $hero_overlay = get_hero_overlay();

    if($hero_overlay) {
        echo '<div class="hero-overlay"></div>';
    }
}

// HERO TITLE - used to add a custom title
function hero_title()
{
    $postID = get_hero_post_id();
    $hero_title = get_field('hero_title', $postID);
    $page_title = get_the_title($postID);

    if($hero_title) {
        echo '<h1 data-appear="fade-right" data-appear-delay="100">' . $hero_title . '</h1>';
    } else if(is_404()) {
        echo '<h1 data-appear="fade-right" data-appear-delay="100">404: Page not found</h1>';
    } else {
        echo '<h1 data-appear="fade-right" data-appear-delay="100">' . $page_title . '</h1>';
    }
}

// HERO SUB TITLE - used to add a custom subtitle
function hero_subtitle()
{
    $postID = get_hero_post_id();
    $hero_subtitle = get_field('hero_subtitle', $postID);

    if($hero_subtitle) {
        echo '<h2 data-appear="fade-right" data-appear-delay="200">' . $hero_subtitle . '</h2>';
    }
    if(is_404()) {
        echo '<h2 data-appear="fade-right" data-appear-delay="200">Oops, looks like that page is missing.</h2>';
    }
}

// HERO INTRO - used to add a custom into to the page
function hero_intro()
{
    $postID = get_hero_post_id();
    $hero_intro = get_field('hero_intro', $postID);

    if($hero_intro) {
        echo '<p data-appear="fade-right" data-appear-delay="300">' . $hero_intro . '</p>';
    }
}