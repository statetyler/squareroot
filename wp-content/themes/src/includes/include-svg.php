<?php // Include SVG
// SVG must be kept in the library/images/svg directory
// simply pass the name of the SVG into the function without the `.svg` extension
function include_svg($svg) {
  include(__DIR__ . '/../library/images/svg/' . $svg .'.svg');
}
?>