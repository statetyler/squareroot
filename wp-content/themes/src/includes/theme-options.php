<?php

/* Add theme options using ACF */
if(function_exists('acf_options_page')) {
	acf_add_options_page(array(
		'page_title'  => 'Theme Options',
		'menu_title'  => 'Theme Options',
		'menu_slug'   => 'theme-options',
		'capability'  => 'edit_posts',
		'redirect'    => false
	));

	// Social Media Links
	acf_add_options_sub_page(array(
		'page_title'  => 'Social Media Settings',
		'menu_title'  => 'Social Media Settings',
		'parent_slug' => 'theme-options',
	));

	// Map
	acf_add_options_sub_page(array(
		'page_title'  => 'Location / Map',
		'menu_title'  => 'Location / Map',
		'parent_slug' => 'theme-options',
	));
}

// Add Popup Settings
if(function_exists('acf_options_page')) {
	acf_add_options_page(array(
		'page_title'  => 'Popup Settings',
		'menu_title'  => 'Popup Settings',
		'menu_slug'   => 'popup-settings',
		'capability'  => 'edit_posts',
		'redirect'    => false
	));
}

?>