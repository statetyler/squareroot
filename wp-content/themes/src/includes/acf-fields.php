<?php // ACF Fields
if(function_exists('acf_add_local_field_group')) {

     ///////////////////////////////
    // Contact Info Settings
    acf_add_local_field_group([
        'key'               		=> 'group_5afcdec262f4a',
        'title'             		=> 'Theme Contact Info',
        'fields'            		=> [
                [
                    'key'       		=> 'field_5b003624ca2cb',
                    'label'     		=> 'Phone Number',
                    'name' 				=> 'phone_number',
                    'type' 				=> 'text',
                    'instructions' 		=> 'Make sure to format the number only using dashes, no spaces or parentheses. Like so: 555-555-5555',
                    'required' 			=> 0,
                    'conditional_logic' => 0,
                    'wrapper' 			=> [
                        'width' 			=> '',
                        'class' 			=> '',
                        'id' 				=> '',
                    ],
                    'default_value' 	=> '555-555-5555',
                    'placeholder' 		=> 'phone number',
                    'prepend' 			=> '',
                    'append' 			=> '',
                    'maxlength' 		=> '',
                ],
                [
                    'key' 				=> 'field_5b004bb56d1fc',
                    'label' 			=> 'Email',
                    'name' 				=> 'email',
                    'type' 				=> 'email',
                    'instructions' 		=> '',
                    'required' 			=> 0,
                    'conditional_logic' => 0,
                    'wrapper' 			=> [
                        'width' 			=> '',
                        'class' 			=> '',
                        'id' 				=> '',
                    ],
                    'default_value' 	=> 'info@buisness.com',
                    'placeholder' 		=> 'email address',
                    'prepend' 			=> '',
                    'append' 			=> '',
                ],
                [
                    'key' 				=> 'field_5b0089fa6d1fd',
                    'label' 			=> 'Address',
                    'name' 				=> 'address',
                    'type' 				=> 'text',
                    'instructions' 		=> '',
                    'required' 			=> 0,
                    'conditional_logic' => 0,
                    'wrapper' 			=> [
                        'width' 			=> '',
                        'class' 			=> '',
                        'id' 				=> '',
                    ],
                    'default_value' 	=> '123 Street Ln. City, ST, 40404',
                    'placeholder' 		=> '',
                    'prepend' 			=> '',
                    'append' 			=> '',
                    'maxlength' 		=> '',
                ],
                [
                    'key' 				=> 'field_5b008b606d1fe',
                    'label' 			=> 'Address Link',
                    'name' 				=> 'address_link',
                    'type' 				=> 'url',
                    'instructions' 		=> 'Add the link to the google maps address.',
                    'required' 			=> 0,
                    'conditional_logic' => 0,
                    'wrapper' 			=> [
                        'width' 			=> '',
                        'class' 			=> '',
                        'id' 				=> '',
                    ],
                    'default_value' 	=> 'https://www.google.com/maps/place/Austin,+TX/@30.3076863,-97.8934868,11z/data=!3m1!4b1!4m5!3m4!1s0x8644b599a0cc032f:0x5d9b464bd469d57a!8m2!3d30.267153!4d-97.7430608',
                    'placeholder' 		=> 'google maps link',
                ],
            ],
            'location'			=> [
                [
                    [
                        'param'		=> 'options_page',
                        'operator' 	=> '==',
                        'value' 	=> 'theme-options',
                    ],
                ],
                [
                    [
                        'param' 	=> 'options_page',
                        'operator' 	=> '==',
                        'value' 	=> 'acf-options-contact-info',
                    ],
                ],
            ],
            'menu_order' 			=> 0,
            'position' 				=> 'normal',
            'style' 				=> 'default',
            'label_placement' 		=> 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' 		=> '',
            'active' 				=> 1,
            'description' 			=> '',
        ]
    );

     ///////////////////////////////
    // Social Media Settings
    acf_add_local_field_group(
        [
            'key'                   => 'group_59bad92ce6f80',
            'title'                 => 'Social Media Settings',
            'fields'                => [
                [
                    'key'               => 'field_59bad95c06fad',
                    'label'             => 'Facebook URL',
                    'name'              => 'facebook_url',
                    'type'              => 'url',
                    'instructions'      => '',
                    'required'          => 0,
                    'conditional_logic' => 0,
                    'wrapper'           => [
                        'width' => '',
                        'class' => '',
                        'id'    => '',
                    ],
                    'default_value'     => '',
                    'placeholder'       => 'Facebook URL',
                ],
                [
                    'key'               => 'field_59bad96f06fae',
                    'label'             => 'Twitter URL',
                    'name'              => 'twitter_url',
                    'type'              => 'url',
                    'instructions'      => '',
                    'required'          => 0,
                    'conditional_logic' => 0,
                    'wrapper'           => [
                        'width' => '',
                        'class' => '',
                        'id'    => '',
                    ],
                    'default_value'     => '',
                    'placeholder'       => 'Twitter URL',
                ],
                [
                    'key'               => 'field_59bad98f06faf',
                    'label'             => 'Instagram URL',
                    'name'              => 'instagram_url',
                    'type'              => 'url',
                    'instructions'      => '',
                    'required'          => 0,
                    'conditional_logic' => 0,
                    'wrapper'           => [
                        'width' => '',
                        'class' => '',
                        'id'    => '',
                    ],
                    'default_value'     => '',
                    'placeholder'       => 'Instagram URL',
                ],
                [
                    'key'               => 'field_59bad99b06fb0',
                    'label'             => 'YouTube URL',
                    'name'              => 'youtube_url',
                    'type'              => 'url',
                    'instructions'      => '',
                    'required'          => 0,
                    'conditional_logic' => 0,
                    'wrapper'           => [
                        'width' => '',
                        'class' => '',
                        'id'    => '',
                    ],
                    'default_value'     => '',
                    'placeholder'       => 'YouTube URL',
                ],
            ],
            'location'              => [
                [
                    [
                        'param'    => 'options_page',
                        'operator' => '==',
                        'value'    => 'theme-options',
                    ],
                ],
                [
                    [
                        'param'    => 'options_page',
                        'operator' => '==',
                        'value'    => 'acf-options-social-media-accounts',
                    ],
                ],
            ],
            'menu_order'            => 1,
            'position'              => 'acf_after_title',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen'        => '',
            'active'                => 1,
            'description'           => 'Theme Options - Social Media',
        ]
    );

     ///////////////////////////////
    // Google Maps API
    acf_add_local_field_group(
        [
            'key'               => 'group_59bad9f16906e',
            'title'             => 'Google Map',
            'fields'            => [
                [
                    'key'               => 'field_59bada04f9e91',
                    'label'             => 'Google Maps API Key',
                    'name'              => 'maps_api',
                    'type'              => 'text',
                    'instructions'      => 'Enter your Google Maps API key.<br>
                                            <strong>You must click \'Update\' after entering the API Key before the Map below will work.</strong><br>
                                            <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">How do I get a Google Maps API Key?</a>',
                    'required'          => 0,
                    'conditional_logic' => 0,
                    'wrapper'           => [
                        'width'             => '',
                        'class'             => '',
                        'id'                => '',
                    ],
                    'default_value'     => '',
                    'placeholder'       => 'Google Maps API Key',
                    'prepend'           => '',
                    'append'            => '',
                    'maxlength'         => '',
                ],
                [
                    'key'               => 'field_59badcb3341f5',
                    'label'             => 'Map',
                    'name'              => 'map',
                    'type'              => 'google_map',
                    'instructions'      => '',
                    'required'          => 0,
                    'conditional_logic' => 0,
                    'wrapper'           => [
                        'width'             => '',
                        'class'             => '',
                        'id'                => '',
                    ],
                    'center_lat'        => '',
                    'center_lng'        => '',
                    'zoom'              => '',
                    'height'            => '',
                ],
            ],
            'location'              => [
                [
                    [
                        'param'         => 'options_page',
                        'operator'      => '==',
                        'value'         => 'theme-options',
                    ],
                ],
                [
                    [
                        'param'         => 'options_page',
                        'operator'      => '==',
                        'value'         => 'acf-options-location-map',
                    ],
                ],
            ],
            'menu_order'            => 2,
            'position'              => 'acf_after_title',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen'        => '',
            'active'                => 1,
            'description'           => 'Theme Options - Google Maps',
        ]
    );
}