<?php // Social Media links
function display_social_media_links() {

    $links = get_social_media_links();

    if(!empty($links)): ?>
    <ul class="social-media-links">
    <?php foreach($links as $platform=>$url) { if(!$url) {continue;} ?>
        <li>
            <a class="icon-<?php echo $platform ?>" title="Find us on <?php echo ucwords($platform) ?>" href="<?php echo $url ?>" target="_blank">
                <i class="fa fa-<?php echo $platform ?>" aria-hidden="true"></i>
            </a>
        </li>
    <?php } ?>
    </ul>
    <?php endif; ?>

<?php }

function get_social_media_links() {
    $array = [];

    $array["facebook"]  = get_field('facebook_url', 'option');
    $array["twitter"]   = get_field('twitter_url', 'option');
    $array["instagram"] = get_field('instagram_url', 'option');
    $array["youtube"]   = get_field('youtube_url', 'option');
    // $array["linkedin"]  = get_field('linkedin_url', 'option');

    return $array;
}