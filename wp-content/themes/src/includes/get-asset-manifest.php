<?php // Asset Manifest

function get_asset_manifest()
{
    return json_decode(file_get_contents(__DIR__ . '/../build/manifest.json'));
}