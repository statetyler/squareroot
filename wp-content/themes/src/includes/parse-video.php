<?php // Parse Video

abstract class Video
{

    public $url;
    public $title;

    protected function __construct($url, $title = '')
    {
        $this->url = $url;
        $this->title = $title;
    }

    /**
     * @return $this
     */
    abstract protected function parseUrl();

    /**
     * @return string
     */
    abstract protected function makeIFrame();

    public static function get($selector = 'video_link')
    {
        $url = get_field($selector);
        $title = get_field('video_title') ?get_subfield('video_title'): '';
        if(!is_string($url)) {
            $url = get_sub_field($selector);
            $title = get_sub_field('video_title') ?get_subfield('video_title'): '';
        }

        $isVimeo = strpos($url, 'vimeo.com');

        if($isVimeo) {
            return (new VimeoVideo($url, $title))->parseUrl()->makeIFrame();
        }

        return (new YoutubeVideo($url, $title))->parseUrl()->makeIFrame();

    }

    public static function hasVideo()
    {
        return (bool)(get_field('video_link'));
    }

}

class YoutubeVideo extends Video
{

    protected function parseUrl()
    {
        if($this->isShortUrl()) {
            $this->url = str_replace('.be', 'be.com/embed', $this->url);

            return $this;
        }

        $this->url = str_replace('www.youtube.com/watch?v=', 'youtu.be', $this->url);
        $this->url = str_replace('.be', 'be.com/embed/', $this->url);

        return $this;
    }

    protected function makeIFrame()
    {
        return '<iframe title="'.$this->title.'" width="560" height="315" src="' . $this->url . '?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen>Your browser does not support iframes.</iframe>';
    }

    private function isShortUrl()
    {
        return strpos($this->url, 'youtu.be');
    }

}

class VimeoVideo extends Video
{

    protected function parseUrl()
    {
        if($this->isEmbedUrl()) {
            return $this;
        }

        $this->url = str_replace('https://', 'https://player.', $this->url);
        $this->url = str_replace('.com/', '.com/video/', $this->url);
        $this->url .= '?title=0&byline=0&portrait=0';

        return $this;
    }

    protected function makeIFrame()
    {
        return "<iframe title=\"{$this->title}\" src=\"{$this->url}\" width=\"640\" height=\"360\" frameborder=\"0\" webkitallowfullscreen
        mozallowfullscreen allowfullscreen></iframe>";
    }

    private function isEmbedUrl()
    {
        return strpos($this->url, 'https://player.vimeo.com/video/');
    }

}